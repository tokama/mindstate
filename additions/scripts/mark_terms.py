import re
from pathlib import Path
from itertools import product
import pymorphy2


def word_forms(morph, word, tag='NOUN'):
    parse_obj = morph.parse(word)

    res_word = parse_obj[0]
    for word_obj in parse_obj:
        if tag in word_obj.tag:
            res_word  = word_obj
            break

    return [x.word for x in res_word.lexeme]


def make_all_phrase_forms(morph, phrase):
    '''Just for two ward phrases'''
    words = phrase.split()
    form_1 = word_forms(morph, words[0], 'ADJF')
    form_2 = word_forms(morph, words[1], 'NOUN')
    forms = []
    for form in product(form_1, form_2):
        forms.append(' '.join(form))

    return forms


def make_glossary(file_name):
    morph = pymorphy2.MorphAnalyzer()
    with open(file_name) as file: gloss = file.read()
    pattern = re.compile(r'\\newglossaryentry{(.+?)}{\s*name=(.+?),\s*description')
    glossary = {}
    for (label, name) in re.findall(pattern, gloss):
        name = name.lower()
        if re.search(r'\s', name):
            forms = make_all_phrase_forms(morph, name)
        else:
            forms = word_forms(morph, name)
        glossary[label] = '|'.join(forms)
    return glossary


def emph_keywords(text, glossary):
    for label, forms in glossary.items():
        text = re.sub(
            r'([^{])\b(%s)\b([^}])' % forms,
            r'\1\\gl{%s}{\2}\3' % label,
            text,
            count=1,
            flags=re.IGNORECASE,
        )

    return text


glossary = make_glossary('glossary.tex')
text_start = re.compile(r'^\\(spar|end{quotation})$')
text_end = re.compile(r'^\\(epar|begin{quotation})$')
book_path = Path('book.tex')
book = ''
text = ''
text_started = False
with book_path.open() as file:
    for line in file.readlines():
        start_matched = text_start.match(line)
        end_matched = text_end.match(line)
        if start_matched:
            text_started = True
        elif end_matched:
            text_started = False
            book += emph_keywords(text, glossary)
            text = ''

        if text_started:
            text += line
        else:
            book += line

book_path.write_text(book)
