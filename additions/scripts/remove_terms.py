import re
from pathlib import Path

book_file = Path('book.tex')
content = book_file.read_text()

content = re.sub(r'\\gl{([^}]+)}{([^}]+)}', r'\2', content)

book_file.write_text(content)
