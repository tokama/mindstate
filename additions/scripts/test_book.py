import regex

with open('book.tex') as file:
    BODY_LINES = file.readlines()
BODY = ''.join(BODY_LINES)

with open('bibliography.bib') as file:
    BIB = file.read()

with open('glossary.tex') as file:
    GLOSS = file.read()


def test_upperccase_after_statement():
    is_upper = regex.compile(r'^\p{Lu}')
    start_statement = regex.compile(r'[^~С].\. (.{30})')
    starts_from_backslash = regex.compile(r'^\s*\\')

    for start in regex.findall(start_statement, BODY):
        if starts_from_backslash.match(start):
            # FIXME check word inside latex command
            continue
        
        assert is_upper.match(start), start


def test_uppercase_at_start():
    is_upper = regex.compile(r'^\p{Lu}')
    is_cyr = regex.compile(r'^\p{IsCyrillic}')
    starts_from_backslash = regex.compile(r'^\s*\\')
    comment = regex.compile(r'^%')
    bracket = regex.compile(r'^}')
    eol = regex.compile(r'^\n')
    etc = regex.compile(r'^...')

    for lineno, line in enumerate(BODY_LINES):
        if not is_cyr.match(line):
            continue

        assert is_upper.match(line), msg(lineno, line)


def test_valid_ref():
    """Проверка валидности ссылок на параграфы."""
    label_pattern = regex.compile(r'label{([^}]+?)}')
    ref_pattern = regex.compile(r'[^h]ref{([^}]+?)}')

    labels = find_all(label_pattern, BODY)
    labels.append('main')  # glossary label

    for ref in regex.findall(ref_pattern, BODY):
        assert ref in labels

    for ref in regex.findall(ref_pattern, GLOSS):
        assert ref in labels


def test_bibliosource():
    """Проверка того, что всё перечисленное в библиографии
    используется в тексте и все использованное в тексте
    есть в библиографии.
    """
    bib_pattern = regex.compile(r'@(?:book|misc){([^,]+?),')
    cite_pattern = regex.compile(r'cite(?:title|){([^}]+?)}')

    bib_sources = find_all(bib_pattern, BIB)
    bib_used = find_all_uniq(cite_pattern, BODY)

    assert bib_sources == bib_used


def test_glossary():
    """Проверки того, что все ссылки на словарь терминов
    указывают на описанные термины и что все термины используются.
    """
    gloss_pattern = regex.compile(r'newglossaryentry{([^}]+?)}')
    term_pattern = regex.compile(r'gl{([^}]+?)}')

    terms = find_all(gloss_pattern, GLOSS)
    used_terms = find_all_uniq(term_pattern, BODY+GLOSS)

    assert terms == used_terms


def test_no_eol_spaces():
    """Проверка отсутствия пробелов на концах строк."""
    space_pattern = regex.compile(r'.*\s+\n')

    for lineno, line in enumerate(BODY_LINES):
        assert not space_pattern.match(line), msg(lineno, line)


def test_last_dot():
    """Проверка наличия правильных символов окончания строки."""
    cyrillic = regex.compile(r'.*\p{Cyrillic}[^}]\n')
    eol_pattern = regex.compile(r'.*[.:?;!]\n')

    for lineno, line in enumerate(BODY_LINES):
        if cyrillic.match(line):
            assert eol_pattern.match(line), msg(lineno, line)


def test_space_after():
    """Проверка наличия пробелов после разделителей."""
    after_pattern = regex.compile(r'\p{Cyrillic}[.,:;?!](.)')
    accepted = [' ', '~', '}', '»', ')', '\\', ',', '.']
    for lineno, line in enumerate(BODY_LINES):
        afters = regex.findall(after_pattern, line)
        is_accepted = [x in accepted for x in afters]
        assert all(is_accepted), msg(lineno, line) + str(afters)


def test_no_space_before():
    """Проверка отсутствия пробелов перед разделителями."""
    space_before_pattern = regex.compile(r'\s+[.,:;?!](?!\.\.)')
    for lineno, line in enumerate(BODY_LINES):
        assert not space_before_pattern.search(line), msg(lineno, line)


def test_no_space_around_hyphen():
    """Проверка отсутствия пробелов вокруг дефиса"""
    left_space_pattern = regex.compile(r'\s-')
    right_space_pattern = regex.compile(r'-\s')
    assert not left_space_pattern.search(BODY)
    assert not right_space_pattern.search(BODY)


def test_no_tab():
    """Проверка отстутствия табуляций."""
    for lineno, line in enumerate(BODY_LINES):
        assert not regex.search(r'\t', line), msg(lineno, line)


def test_no_simple_quotes():
    """Проверка отстутствия обычных кавычек."""
    assert not regex.search(r'"', BODY)


def find_all(pattern, line):
    """Поиск всех совпадение регекса и возврат отсортированного списка"""
    res = list(regex.findall(pattern, line))
    res.sort()
    return res


def find_all_uniq(pattern, line):
    """Поиск всех уникальных совпадение регекса
    и возврат отсортированного списка
    """
    res = list(set(regex.findall(pattern, line)))
    res.sort()
    return res


def msg(lineno, line):
    return '{}: {}'.format(lineno+1, line)
